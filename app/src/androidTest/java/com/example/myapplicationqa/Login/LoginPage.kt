package com.example.myapplicationqa.Login

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.example.myapplicationqa.Login.Robot.RobotLogin
import com.example.myapplicationqa.MainActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class LoginPage {

    private val robot = RobotLogin()

    @get:Rule
    var mActivityRule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java)

    @Test
    fun testeLogin() {
        robot.escreverEmail()
        robot.escreverSenha()
        robot.validarTextLogin()

    }
}