package com.example.myapplicationqa.Login.Robot

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.example.myapplicationqa.Login.Constantes.ConstanteLogin.EMAIL_SUCESSO
import com.example.myapplicationqa.Login.Constantes.ConstanteLogin.SENHA_SUCESSO
import com.example.myapplicationqa.Login.Constantes.ConstanteLogin.TEXT_LOGIN
import com.example.myapplicationqa.R
import com.example.myapplicationqa.Utils

class RobotLogin {
    private val utils = Utils()

    fun escreverEmail() {
        onView(withId(CAMPO_EMAIL))
            .perform(typeText(EMAIL_SUCESSO))
    }

    fun  escreverSenha() {
        onView(withId(CAMPO_SENHA))
            .perform(typeText(SENHA_SUCESSO))
    }

    fun validarTextLogin() {
        onView(withText(TEXT_LOGIN)).check(matches(isDisplayed()))
        onView(withId(ID_TEXTO_LOGIN)).check(matches(withText(TEXT_LOGIN)))
        utils.ValidarTexto("Olá, seja bem vindo!")
        utils.ValidarTexto(TEXT_LOGIN)
        utils.EscreverTexto(CAMPO_EMAIL, EMAIL_SUCESSO)
    }




    companion object {
        private val CAMPO_EMAIL = R.id.editTextTextEmailAddress
        private val CAMPO_SENHA = R.id.editTextTextPassword
        private val ID_TEXTO_LOGIN = R.id.textview_home
     }
}