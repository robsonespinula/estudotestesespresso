package com.example.myapplicationqa

import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import com.example.myapplicationqa.Login.Constantes.ConstanteLogin
import com.example.myapplicationqa.Login.Robot.RobotLogin

class Utils {
    fun ValidarTexto(txt: String) {
        onView(withText(txt))
            .check(matches(isDisplayed()))
    }

    fun EscreverTexto(id: Int, txt: String){
        onView(withId(id))
            .perform(typeText(txt))
    }


}