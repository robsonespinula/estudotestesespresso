package com.example.myapplicationqa

import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Rule


@RunWith(AndroidJUnit4::class)
class ModoBasico {

    @get:Rule
    var mActivityRule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java)

    @Test
    fun testeLogin() {
        onView(withText("Login")).perform(click())


        Espresso.pressBack()
        onView(withId(R.id.button_first)).perform(click())
    }

    @Test
    fun testeLogin2() {
        onView(withId(R.id.editTextTextEmailAddress)).perform(typeText("robsons@teste.com"))
        onView(withId(R.id.editTextTextPassword)).perform(typeText("123456"))
        Espresso.closeSoftKeyboard()
        onView(withId(R.id.button_first)).perform(click())
        onView(withText("Você está na Home")).check(matches(isDisplayed()))
    }

    @Test
    fun testeLoginFalha() {
        onView(withId(R.id.editTextTextEmailAddress)).perform(typeText("robsons@teste.com"))
        onView(withId(R.id.editTextTextPassword)).perform(typeText("123456"))
        Espresso.closeSoftKeyboard()
        onView(withId(R.id.button_first)).perform(click())
        onView(withText("Você está na XXXXX")).check(matches(isDisplayed()))
    }
}